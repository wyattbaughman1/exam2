// Exam2
// Wyatt Baughman

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

double FindAverage(double Number[]);

int main()
{
	cout << "Please enter 5 numbers" << "\n";

	double num[5];

	for (int i = 0; i < 5; i++)
	{
		cout << "Number: ";
		cin >> num[i];
	}

	cout << "\n";

	for (int i = 0; i < 5; i++)
	{
		cout << num[i]++ << " ";
	}

	cout << "\n";

	double Average = 0;

	Average = FindAverage(num);

	cout << "Average: " << Average << "\n";


	double max = num[0];

	for (int i = 0; i < 5; i++)
	{
		if (num[i] > max)
		{
			max = num[i];
		}
	}

	cout << "Maximum: " << max - 1 << "\n";

	double min = num[0];

	for (int i = 0; i < 5; i++)
	{
		if (num[i] < min)
		{
			min = num[i];
		}
	}

	cout << "Minimum: " << min - 1 << "\n";

	string path = "Exam2.txt";

	string y;

	cout << "Would you like to save output to file? " << "(y/n):"; cin >> y;

	if (y == "y" || y == "Y")
	{
		cout << "Results are being saved to Exam2.txt";

		ofstream ofs(path);

		ofs << "Average: " << Average << "\n";
		ofs << "Maximum: " << max - 1 << "\n";
		ofs << "Minimum: " << min - 1 << "\n";

		ofs.close();
	}

	(void)_getch();
	return 0;
}

double FindAverage(double Number[])
{
	double sum = 0;
	double avg = 0;

	for (int i = 0; i < 5; i++)
	{
		sum += Number[i];
	}

	avg = (sum - 5) / 5;

	return avg;
}
